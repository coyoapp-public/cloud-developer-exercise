# COYO Cloud Developer exercise

This project contains sample code for a Java application using the [Spring Boot](https://spring.io/) framework. The goal of the Cloud Developer exercise is to create and run a Docker container for the sample application using Docker Compose.

The sample application is built using [Gradle](https://gradle.org) and provides some endpoints on port `8080`:

* `/hello` which is the main application
* `/actuator/health` which can be used for a health check

The sample application can be run using `./gradlew`:

```bash
./gradlew bootRun
```

## Requirements

* [Docker](https://www.docker.com)
* [Docker Compose](https://docs.docker.com/compose/)
* recommended: a Java JDK >= 11, i.e. [OpenJDK](https://openjdk.java.net/install/)
* optional, if not using provided `./gradlew`: [Gradle](https://gradle.org/) 5 (5.6.x only) or 6 (6.3 or later)

## Tasks

* Containerize the sample application using Docker
* Run the containerized sample application using Docker Compose including a health check on `/actuator/health`

## Expected outcome

* a **`.zip`** file containing a `Dockerfile` and a `docker-compose.yml`
